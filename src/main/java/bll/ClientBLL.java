package bll;

import bll.validator.EmailValidator;
import bll.validator.Validator;
import dao.ClientDao;
import model.Client;

import javax.swing.table.DefaultTableModel;
import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;
/**
 * Created by Amacus5 on 23.04.2017.
 */
public class ClientBLL {
    private List<Validator<Client>> validators;

    public ClientBLL() {
        validators = new ArrayList<Validator<Client>>();
        validators.add(new EmailValidator());
    }

    public Client findClientById(int id) {
        Client st = ClientDao.findById(id);
        if (st == null) {
            throw new NoSuchElementException("The Client with id =" + id + " was not found!");
        }
        return st;
    }

    public int insertClient(Client Client) {
        for (Validator<Client> v : validators) {
            v.validate(Client);
        }
        return ClientDao.insert(Client);
    }
    public int deleteClient(int id) {
        ClientDao.delete(id);
        return id;
    }
    public DefaultTableModel viewClient() {
        return ClientDao.view();

    }
    public int editClient(Client Client) {
        ClientDao.edit(Client);
        return Client.getId();
    }
}
