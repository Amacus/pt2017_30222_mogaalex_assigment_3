package dao;

import connection.ConnectionFactory;
import model.Product;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.sql.*;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by Amacus5 on 23.04.2017.
 */
public class ProductDao {
    protected static final Logger LOGGER = Logger.getLogger(ProductDao.class.getName());
    private static final String insertStatementString = "INSERT INTO Product (productName,productPrice,productStock)"
            + " VALUES (?,?,?)";
    private final static String findStatementString = "SELECT * FROM Product where idProduct = ?";
    private final static String deleteStatementString = "DELETE FROM Product WHERE idProduct = ?";
    private final static String viewStatementString ="select * from product";
    private final static String editStatementString ="UPDATE product " +
            "SET productName = ?,productPrice= ?,productStock = ? WHERE idProduct = ?";
    public static Product findById(int ProductId) {
        Product toReturn = null;

        Connection dbConnection = ConnectionFactory.getConnection();
        PreparedStatement findStatement = null;
        ResultSet rs = null;
        try {
            findStatement = dbConnection.prepareStatement(findStatementString);
            findStatement.setLong(1, ProductId);
            rs = findStatement.executeQuery();
            rs.next();

            String name = rs.getString("productName");
            int price = rs.getInt("productPrice");
            int stock = rs.getInt("productStock");
            toReturn = new Product(ProductId, name, price, stock);
        } catch (SQLException e) {
            LOGGER.log(Level.WARNING, "ProductDAO:findById " + e.getMessage());
        } finally {
            ConnectionFactory.close(rs);
            ConnectionFactory.close(findStatement);
            ConnectionFactory.close(dbConnection);
        }
        return toReturn;
    }


    public static int insert(Product Product) {
        Connection dbConnection = ConnectionFactory.getConnection();

        PreparedStatement insertStatement = null;
        int insertedId = -1;
        try {
            insertStatement = dbConnection.prepareStatement(insertStatementString, Statement.RETURN_GENERATED_KEYS);
            insertStatement.setString(1, Product.getName());
            insertStatement.setInt(2, Product.getprice());
            insertStatement.setInt(3, Product.getstock());
            insertStatement.executeUpdate();

            ResultSet rs = insertStatement.getGeneratedKeys();
            if (rs.next()) {
                insertedId = rs.getInt(1);
            }
            JOptionPane.showMessageDialog(null, "Success");
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, "Failed");
        } finally {
            ConnectionFactory.close(insertStatement);
            ConnectionFactory.close(dbConnection);
        }
        return insertedId;
    }

    public static int delete(int id) {
        Connection dbConnection = ConnectionFactory.getConnection();

        PreparedStatement deleteStatement = null;
        try {
            deleteStatement = dbConnection.prepareStatement(deleteStatementString);
            deleteStatement.setInt(1, id);

            // execute delete SQL statement
            deleteStatement.executeUpdate();
            JOptionPane.showMessageDialog(null, "Success");
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            ConnectionFactory.close(deleteStatement);
            ConnectionFactory.close(dbConnection);
        }
        return id;
    }
    public static int edit(Product product) {
        Product toReturn = null;

        Connection dbConnection = ConnectionFactory.getConnection();
        PreparedStatement editStatement = null;
        try {
            editStatement = dbConnection.prepareStatement(editStatementString);
            editStatement.setString(1, product.getName());
            editStatement.setInt(2, product.getprice());
            editStatement.setInt(3, product.getstock());
            editStatement.setInt(4, product.getId());
            editStatement.executeUpdate();
            JOptionPane.showMessageDialog(null, "Success");

        }catch (Exception e){
            e.printStackTrace();
            JOptionPane.showMessageDialog(null, "Failed");
        } finally {
            ConnectionFactory.close(editStatement);
            ConnectionFactory.close(dbConnection);
        }
        return product.getId();
    }
    public static DefaultTableModel view() {
        Connection dbConnection = ConnectionFactory.getConnection();
        PreparedStatement viewStatement = null;
        ResultSet rs = null;
        Vector<String> columnNames = new Vector<String>();
        Vector<Vector<Object>> data = new Vector<Vector<Object>>();
        try {
            viewStatement = dbConnection.prepareStatement(viewStatementString);
        rs = viewStatement.executeQuery();
            ResultSetMetaData metaData = rs.getMetaData();

            // names of columns
            int columnCount = metaData.getColumnCount();
            for (int column = 1; column <= columnCount; column++) {
                columnNames.add(metaData.getColumnName(column));
            }

            // data of the table
            while (rs.next()) {
                Vector<Object> vector = new Vector<Object>();
                for (int columnIndex = 1; columnIndex <= columnCount; columnIndex++) {
                    vector.add(rs.getObject(columnIndex));
                }
                data.add(vector);
            }
        } catch (SQLException e) {
            e.printStackTrace();
            JOptionPane.showMessageDialog(null, "Failed");
        }

            ConnectionFactory.close(viewStatement);
            ConnectionFactory.close(dbConnection);

        return new DefaultTableModel(data, columnNames);
    }
}

