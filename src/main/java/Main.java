import presentation.OrderView;
import presentation.ProductView;
import presentation.View;

import javax.swing.*;
import java.sql.SQLException;

/**
 * Created by Amacus5 on 22.04.2017.
 */
public class Main {
    public static void main(String[] args) throws SQLException {
        View view = new View();
        JFrame frame = new JFrame("Client");
        frame.setContentPane( view.panel1);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);

        ProductView pview = new ProductView();
        JFrame frame2 = new JFrame("Product");
        frame2.setContentPane( pview.panel1);
        frame2.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame2.pack();
        frame2.setVisible(true);

        OrderView oview = new OrderView();
        JFrame frame3 = new JFrame("Order");
        frame3.setContentPane( oview.panel1);
        frame3.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame3.pack();
        frame3.setVisible(true);

       /* ClientBLL ClientBll = new ClientBLL();
         id = ClientBll.insertClient(client);
        if (id > 0) {
            ClientBll.findClientById(id);
        }


        // Generate error
        try {
            ClientBll.findClientById(1);
        } catch (Exception ex) {
            LOGGER.log(Level.INFO, ex.getMessage());
        }*/



    }



}

