package dao;

import connection.ConnectionFactory;
import model.Client;
import model.Order;
import model.Product;

import javax.swing.*;
import java.io.PrintWriter;
import java.sql.*;
import java.util.ArrayList;

/**
 * Created by Amacus5 on 26.04.2017.
 */
public class OrderDao {
    private final static String insertStatementString = "INSERT INTO `order`( nameClient,nameProduct,price,product_idProduct,client_idClient)" +
            "SELECT nameClient, product.productname,product.productPrice,product.idProduct,client.idClient from client join product on client.idClient = ? and product.idProduct = ?";
    private final static String decrementStatementString ="update product set productstock = productstock-? where idProduct=?";
    private final static String selectStatementString = "SELECT nameclient, nameproduct,price FROM `order` WHERE client_idclient=?";
    private final static String priceStatementString = "SELECT SUM(price) FROM `order` where client_idclient=?";
    public static void decrement(Product Product,int quantity) {
        Connection dbConnection = ConnectionFactory.getConnection();
        PreparedStatement decrementStatement = null;
        try {
            decrementStatement = dbConnection.prepareStatement(decrementStatementString);
            decrementStatement.setInt(1, quantity);
            decrementStatement.setInt(2, Product.getId());
            decrementStatement.executeUpdate();
            JOptionPane.showMessageDialog(null, "Success");
        }catch (SQLException e) {
            JOptionPane.showMessageDialog(null, "Failed");
        } finally {
            ConnectionFactory.close(decrementStatement);
            ConnectionFactory.close(dbConnection);
        }

    }
    public static int insert(Product Product, Client Client,int quantity) {
        Connection dbConnection = ConnectionFactory.getConnection();

        PreparedStatement insertStatement = null;
        int insertedId = -1;
        try {
            if(Product.getstock()-quantity>=0) {
                insertStatement = dbConnection.prepareStatement(insertStatementString, Statement.RETURN_GENERATED_KEYS);
                insertStatement.setInt(1, Client.getId());
                insertStatement.setInt(2, Product.getId());
                insertStatement.executeUpdate();

                ResultSet rs = insertStatement.getGeneratedKeys();
                if (rs.next()) {
                    insertedId = rs.getInt(1);
                }
                decrement(Product,quantity);
                JOptionPane.showMessageDialog(null, "Success");
            }
            else{
                JOptionPane.showMessageDialog(null, "Out of stock");
            }
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, "Failed");
        } finally {
            ConnectionFactory.close(insertStatement);
            ConnectionFactory.close(dbConnection);
        }
        return insertedId;
    }
    public static void writeTxt(Order order) {
        ArrayList <String> result = new ArrayList<String>();
        ArrayList <String> result2 = new ArrayList<String>();
        String result3 = new String();
        Connection dbConnection = ConnectionFactory.getConnection();
        PreparedStatement selectStatement = null;
        PreparedStatement priceStatement = null;
        ResultSet rs = null;
        ResultSet rs2 = null;
        try {
            selectStatement = dbConnection.prepareStatement(selectStatementString);
            selectStatement.setInt(1, order.getIdClient());
            rs = selectStatement.executeQuery();
            priceStatement = dbConnection.prepareStatement(priceStatementString);
            priceStatement.setInt(1, order.getIdClient());
            rs2 = priceStatement.executeQuery();
            PrintWriter writer = new PrintWriter("the-file-name.txt", "UTF-8");
            while(rs.next()){
                order.setNameClient(rs.getString(1));
                result.add(rs.getString(2));
                result2.add(rs.getString(3));
            }
            rs2.next();
            result3=rs2.getString(1);
            writer.println("Name: "+order.getNameClient());
            writer.println("Products           Price");

            for (int i=0; i<result.size() ; i++) {


                writer.println(result.get(i)+"              "+result2.get(i));
            }
            writer.println("Total price: " +result3 );
            writer.close();
            JOptionPane.showMessageDialog(null, "Success");
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "Failed writeTxt");
        }
    }
}
