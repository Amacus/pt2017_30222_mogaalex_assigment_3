package bll;

import bll.validator.Validator;
import dao.ProductDao;
import model.Client;
import model.Product;

import javax.swing.table.DefaultTableModel;
import java.util.List;
import java.util.NoSuchElementException;

/**
 * Created by Amacus5 on 23.04.2017.
 */
public class ProductBLL {
    private List<Validator<Client>> validators;

    public ProductBLL() {
    }

    public Product findProductById(int id) {
        Product st = ProductDao.findById(id);
        if (st == null) {
            throw new NoSuchElementException("The Product with id =" + id + " was not found!");
        }
        return st;
    }

    public int insertProduct(Product product) {
        return ProductDao.insert(product);
    }
    public int deleteProduct(int id) {
         ProductDao.delete(id);
         return id;
    }
    public DefaultTableModel viewProduct() {
            return ProductDao.view();

    }
    public int editProduct(Product product) {
        ProductDao.edit(product);
        return product.getId();
    }
}
