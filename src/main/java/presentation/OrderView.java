package presentation;

import bll.ClientBLL;
import bll.OrderBLL;
import bll.ProductBLL;
import model.Client;
import model.Order;
import model.Product;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Created by Amacus5 on 26.04.2017.
 */
public class OrderView {
    private JButton buyButton;
    private JTextField idClientTextField;
    private JTextField idProductTextField;
    private JTextField quantityTextField;
    public JPanel panel1;
    private JButton createTextFileButton;

    public OrderView() {
        buyButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    int idClient = Integer.parseInt(idClientTextField.getText());
                    int idProduct = Integer.parseInt(idProductTextField.getText());
                    int quantity = Integer.parseInt(quantityTextField.getText());
                    ProductBLL productBll = new ProductBLL();
                    ClientBLL clientBll = new ClientBLL();
                    OrderBLL orderBll = new OrderBLL();
                    Client client = clientBll.findClientById(idClient);
                    Product product = productBll.findProductById(idProduct);
                    orderBll.insertOrder(product, client,quantity);
                } catch (Exception ex) {
                    JOptionPane.showMessageDialog(null, "Failed");
                }


            }
        });
        createTextFileButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    int idClient = Integer.parseInt(idClientTextField.getText());
                    int idProduct = Integer.parseInt(idProductTextField.getText());
                    int q = Integer.parseInt(quantityTextField.getText());
                    Order order = new Order("Alex", "ss", 0, idProduct, idClient);
                    OrderBLL orderBll = new OrderBLL();
                    orderBll.writeTxt(order);
                }catch(Exception ex){
                    JOptionPane.showMessageDialog(null, "Failed");
                }
            }
        });

    }

}
