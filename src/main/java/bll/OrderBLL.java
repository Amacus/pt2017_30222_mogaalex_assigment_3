package bll;

import dao.OrderDao;
import model.Client;
import model.Order;
import model.Product;

/**
 * Created by Amacus5 on 26.04.2017.
 */
public class OrderBLL {
    public int insertOrder(Product product, Client client,int q) {
        return OrderDao.insert(product,client,q);
    }
    public void writeTxt(Order order) {
         OrderDao.writeTxt(order);
    }
}
