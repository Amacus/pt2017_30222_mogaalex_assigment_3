package model;

/**
 * Created by Amacus5 on 22.04.2017.
 */
public class Client {
    private int id;
    private String name;
    private String city;
    private String email;

    public Client(int id, String name, String email, String city) {
        super();
        this.id = id;
        this.name = name;
        this.city = city;
        this.email = email;
    }

    public Client(String name, String email, String city) {
        super();
        this.name = name;
        this.city = city;
        this.email = email;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getcity() {
        return city;
    }

    public void setcity(String city) {
        this.city = city;
    }
    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Override
    public String toString() {
        return "Client [id=" + id + ", name=" + name + ", email=" + email + ", city=" + city + "]";
    }

}