package model;

/**
 * Created by Amacus5 on 26.04.2017.
 */
public class Order {
    private int id;
    private String nameClient;
    private String nameProduct;
    private int price;
    private int idProduct;
    private int idClient;

    public Order(int id, String nameClient, String nameProduct, int price, int idProduct,int idClient) {
        super();
        this.id = id;
        this.nameClient = nameClient;
        this.nameProduct = nameProduct;
        this.price = price;
        this.idProduct = idProduct;
        this.idClient = idClient;
    }

    public Order(String nameClient, String nameProduct, int price, int idProduct,int idClient) {
        super();
        this.nameClient = nameClient;
        this.nameProduct = nameProduct;
        this.price = price;
        this.idProduct = idProduct;
        this.idClient = idClient;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNameClient() {
        return nameClient;
    }

    public void setNameClient(String nameClient) {
        this.nameClient = nameClient;
    }

    public String getNameProduct() {
        return nameProduct;
    }

    public void setNameProduct(String nameProduct) {
        this.nameProduct = nameProduct;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public int getIdProduct() {
        return idProduct;
    }

    public void setIdProduct(int idProduct) {
        this.idProduct = idProduct;
    }

    public int getIdClient() {
        return idClient;
    }

    public void setIdClient(int idClient) {
        this.idClient = idClient;
    }






    @Override
    public String toString() {
        return "Order [id=" + id + ", nameClient=" + nameClient + ", nameProduct=" + nameProduct + ", price=" + price + "]";
    }
}
