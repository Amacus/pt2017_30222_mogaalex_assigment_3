package bll.validator;

/**
 * Created by Amacus5 on 23.04.2017.
 */
public interface Validator<T> {

    public void validate(T t);
}
