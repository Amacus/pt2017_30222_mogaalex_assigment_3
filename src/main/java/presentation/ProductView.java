package presentation;

import bll.ProductBLL;
import model.Product;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Created by Amacus5 on 23.04.2017.
 */
public class ProductView {
    private JButton insertButton;
    private JButton deleteButton;
    private JButton editButton;
    private JButton viewButton;
    private JTextField nameTextField;
    private JTextField priceTextField;
    private JTextField stockTextField;
    private JTextField idTextField;
    public JPanel panel1;
    private JTable table1;

    public ProductView() {
        insertButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Product Product = new Product(nameTextField.getText(), Integer.parseInt(priceTextField.getText()),Integer.parseInt(stockTextField.getText()));
                ProductBLL ProductBLL = new ProductBLL();
                int id = ProductBLL.insertProduct(Product);
                if (id > 0) {
                    ProductBLL.findProductById(id);
                }


                // Generate error
                try {
                    ProductBLL.findProductById(id);
                } catch (Exception ex) {
                    JOptionPane.showMessageDialog(null, "Failed");
                }
            }
        });
        deleteButton.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent e) {
                    try {
                        ProductBLL productBll = new ProductBLL();
                        int id = Integer.parseInt(idTextField.getText());
                        productBll.findProductById(id);
                        productBll.deleteProduct(Integer.parseInt(idTextField.getText()));
                    }catch(Exception ex){
                        JOptionPane.showMessageDialog(null, "ERROR: Invalid id");
                    }
                
            }
        });
        editButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    ProductBLL productBll = new ProductBLL();
                    int id = Integer.parseInt(idTextField.getText());
                    String name = nameTextField.getText();
                    int price = Integer.parseInt(priceTextField.getText());
                    int stock = Integer.parseInt(stockTextField.getText());
                    Product product = new Product(id,name,price,stock);
                    productBll.findProductById(id);
                    productBll.editProduct(product);
                }catch(Exception ex){
                    JOptionPane.showMessageDialog(null, "ERROR: Invalid id");
                }
            }
        });
        viewButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                ProductBLL productBll = new ProductBLL();
                    table1.setModel(productBll.viewProduct());

            }
        });
    }

}

