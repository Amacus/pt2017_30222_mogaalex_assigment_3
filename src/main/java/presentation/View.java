package presentation;

import bll.ClientBLL;
import model.Client;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Created by Amacus5 on 23.04.2017.
 */
public class View {
    private JButton insertButton;
    private JButton deleteButton;
    private JButton editButton;
    private JButton viewButton;
    private JTextField nameTextField;
    private JTextField emailTextField;
    private JTextField cityTextField;
    public JPanel panel1;
    private JTextField idTextField;
    private JTable table1;

    public View() {
        insertButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Client client = new Client(nameTextField.getText(), emailTextField.getText(),cityTextField.getText());
                ClientBLL clientBLL = new ClientBLL();
                int id = clientBLL.insertClient(client);
                if (id > 0) {
                    clientBLL.findClientById(id);
                }


                // Generate error
                try {
                    clientBLL.findClientById(id);
                } catch (Exception ex) {
                    JOptionPane.showMessageDialog(null, "Failed");
                }

            }
        });
        deleteButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    ClientBLL ClientBll = new ClientBLL();
                    int id = Integer.parseInt(idTextField.getText());
                    ClientBll.findClientById(id);
                    ClientBll.deleteClient(Integer.parseInt(idTextField.getText()));
                }catch(Exception ex){
                    JOptionPane.showMessageDialog(null, "ERROR: Invalid id");
                }

            }
        });
        editButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    ClientBLL ClientBll = new ClientBLL();
                    int id = Integer.parseInt(idTextField.getText());
                    String name = nameTextField.getText();
                    String email = emailTextField.getText();
                    String city = cityTextField.getText();
                    Client Client = new Client(id,name,email,city);
                    ClientBll.findClientById(id);
                    ClientBll.editClient(Client);
                }catch(Exception ex){
                    JOptionPane.showMessageDialog(null, "ERROR: Invalid id");
                }


            }
        });
        viewButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                ClientBLL clientBll = new ClientBLL();
                table1.setModel(clientBll.viewClient());

            }
        });

    }

}
