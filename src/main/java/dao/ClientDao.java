package dao;
import connection.ConnectionFactory;
import model.Client;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.sql.*;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;
/**
 * Created by Amacus5 on 22.04.2017.
 */
public class ClientDao {
    protected static final Logger LOGGER = Logger.getLogger(ClientDao.class.getName());
    private static final String insertStatementString = "INSERT INTO Client (nameClient,email,city)"
            + " VALUES (?,?,?)";
    private final static String findStatementString = "SELECT * FROM Client where idClient = ?";
    private final static String deleteStatementString = "DELETE FROM client WHERE idClient = ?";
    private final static String viewStatementString ="select * from client";
    private final static String editStatementString ="UPDATE Client " +
            "SET nameClient = ?,email= ?,city = ? WHERE idClient = ?";

    public static Client findById(int ClientId) {
        Client toReturn = null;

        Connection dbConnection = ConnectionFactory.getConnection();
        PreparedStatement findStatement = null;
        ResultSet rs = null;
        try {
            findStatement = dbConnection.prepareStatement(findStatementString);
            findStatement.setLong(1, ClientId);
            rs = findStatement.executeQuery();
            rs.next();

            String name = rs.getString("nameClient");
            String city = rs.getString("city");
            String email = rs.getString("email");
            toReturn = new Client(ClientId, name, email, city);
            JOptionPane.showMessageDialog(null, "Success");
        } catch (SQLException e) {
            LOGGER.log(Level.WARNING,"ClientDAO:findById " + e.getMessage());
            JOptionPane.showMessageDialog(null, "Failed");
        } finally {
            ConnectionFactory.close(rs);
            ConnectionFactory.close(findStatement);
            ConnectionFactory.close(dbConnection);
        }
        return toReturn;
    }



    public static int insert(Client Client) {
        Connection dbConnection = ConnectionFactory.getConnection();

        PreparedStatement insertStatement = null;
        int insertedId = -1;
        try {
            insertStatement = dbConnection.prepareStatement(insertStatementString, Statement.RETURN_GENERATED_KEYS);
            insertStatement.setString(1, Client.getName());
            insertStatement.setString(2, Client.getEmail());
            insertStatement.setString(3, Client.getcity());
            insertStatement.executeUpdate();

            ResultSet rs = insertStatement.getGeneratedKeys();
            if (rs.next()) {
                insertedId = rs.getInt(1);
            }
            JOptionPane.showMessageDialog(null, "Success");
        } catch (SQLException e) {
            LOGGER.log(Level.WARNING, "ClientDAO:insert " + e.getMessage());
            JOptionPane.showMessageDialog(null, "Failed");
        } finally {
            ConnectionFactory.close(insertStatement);
            ConnectionFactory.close(dbConnection);
        }
        return insertedId;
    }
    public static int delete(int id) {
        Connection dbConnection = ConnectionFactory.getConnection();

        PreparedStatement deleteStatement = null;
        try {
            deleteStatement = dbConnection.prepareStatement(deleteStatementString);
            deleteStatement.setInt(1, id);

            // execute delete SQL statement
            deleteStatement.executeUpdate();
            JOptionPane.showMessageDialog(null, "Succes");
        } catch (SQLException e) {
            e.printStackTrace();
            JOptionPane.showMessageDialog(null, "Failed");
        } finally {
            ConnectionFactory.close(deleteStatement);
            ConnectionFactory.close(dbConnection);
        }
        return id;
    }
    public static int edit(Client Client) {
        Client toReturn = null;

        Connection dbConnection = ConnectionFactory.getConnection();
        PreparedStatement editStatement = null;
        ResultSet rs = null;
        try {
            editStatement = dbConnection.prepareStatement(editStatementString);
            editStatement.setString(1, Client.getName());
            editStatement.setString(2, Client.getEmail());
            editStatement.setString(3, Client.getcity());
            editStatement.setInt(4, Client.getId());
            editStatement.executeUpdate();
            JOptionPane.showMessageDialog(null, "Succes");

        }catch (Exception e){
            e.printStackTrace();
            JOptionPane.showMessageDialog(null, "Failed");
        } finally {
            ConnectionFactory.close(editStatement);
            ConnectionFactory.close(dbConnection);
        }
        return Client.getId();
    }
    public static DefaultTableModel view() {
        Connection dbConnection = ConnectionFactory.getConnection();
        PreparedStatement viewStatement = null;
        ResultSet rs = null;
        Vector<String> columnNames = new Vector<String>();
        Vector<Vector<Object>> data = new Vector<Vector<Object>>();
        try {
            viewStatement = dbConnection.prepareStatement(viewStatementString);
            rs = viewStatement.executeQuery();
            ResultSetMetaData metaData = rs.getMetaData();

            // names of columns
            int columnCount = metaData.getColumnCount();
            for (int column = 1; column <= columnCount; column++) {
                columnNames.add(metaData.getColumnName(column));
            }

            // data of the table
            while (rs.next()) {
                Vector<Object> vector = new Vector<Object>();
                for (int columnIndex = 1; columnIndex <= columnCount; columnIndex++) {
                    vector.add(rs.getObject(columnIndex));
                }
                data.add(vector);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        ConnectionFactory.close(viewStatement);
        ConnectionFactory.close(dbConnection);

        return new DefaultTableModel(data, columnNames);
    }
}
